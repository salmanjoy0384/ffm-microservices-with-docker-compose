# FFM Microservices with Docker-Compose

Name
Deployment of Field Force Management Microservices with the help of Docker and docker-compose

Description
Here is a complete codebase for the FFM microservices in use for users.
We have created Dockerfiles for these services, and then deployed all these in a single deployment using docker-compose. We have also used Postgresql as the Database to store data.
Every services work on their own, and are only concerned about the tasks that are given to them.
All of these services are of either Javascript stack or Python stack(Authentication and Task services). The JS frameworks are of React and NextJS, while Python frameworks are of Flask.
Therefore, we have used the alpine or slim versions of the their technology_stack-specific base images, and built upon them. This way, we have a minimal image which works following the principle of microservices.
For deploying the services, there is no particular sequence, but we need to deploy the services with database migration enabled. In this case, authentication and user management services need to be deployed first, as the migration of data is also needed to be initialized. Please remember that we have to deploy PostgreSQL at the very first, and create necessary environment and configure as per requirement.

The docker-compose file then runs all the docker containers built using images with the help of these dockerfiles, with the addition of Proxy servers using NGINX and Postgresql as the database. So, after deploying the services, we can use the IP address of the server we are on, and then access the microservices with their specific routing with the NGINX proxy container, using specific ports. By default, users will be routed to the frontend of the service.

USAGE
The Authentication service controls all the authenticaion from the mobile app or website, and handles the registration of users as well.
The User Management service manages all the users(company employees).
The Task service helps to assign tasks and handles these services.
The Delivery service has a feature where users will be able to assign a specific person as the delivery person to deliver anything to specific place.
The Attendance service will handle all the attendance related stuffs, including the tasks.

These are all the backend microservices that are in use, all of which can be accessed using the frontend service.